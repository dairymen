class CreatePlans < ActiveRecord::Migration
  def change
    create_table :plans do |t|
      t.date :start_date, :null => false
      t.date :end_date
      t.text :content, :null => false
      t.references :cow, :null => false

      t.timestamps
    end
  end
end
