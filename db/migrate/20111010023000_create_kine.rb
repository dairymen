class CreateKine < ActiveRecord::Migration
  def change
    create_table :kine do |t|
      t.string :ear_tag
      t.string :cage
      t.date :birthday

      t.timestamps
    end
  end
end
