class SeminationEstrusIdAllowNull < ActiveRecord::Migration
  def up
    change_column(:seminations, :estrus_id, :integer, :null => true)
  end

  def down
    change_column(:seminations, :estrus_id, :integer, :null => false)
  end
end
