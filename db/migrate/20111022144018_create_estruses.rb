class CreateEstruses < ActiveRecord::Migration
  def change
    create_table :estruses do |t|
      t.integer :cow_id
      t.date :date

      t.timestamps
    end
  end
end
