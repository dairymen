class AddApprovalNumberToCow < ActiveRecord::Migration
  def change
    add_column :kine, :approval_number, :integer
    Cow.reset_column_information
    Cow.all.each do |cow|
      cow.update_attribute(:approval_number, cow.id)
    end
    change_column :kine, :approval_number, :integer, :null => false
  end
end
