class AddUniqueConditions < ActiveRecord::Migration
  def change
    change_column :seminations, :cow_id, :integer, :null => false
    change_column :seminations, :estrus_id, :integer, :null => false
    change_column :seminations, :date, :date, :null => false
    change_column :seminations, :bull, :string, :limit => 32, :null => false
    change_column :estruses, :cow_id, :integer, :null => false
    change_column :estruses, :date, :date, :null => false
    change_column :kine, :ear_tag, :string, :limit => 16, :null => false
    change_column :kine, :stall_id, :integer, :null => false
    change_column :kine, :birthday, :date, :null => false
    change_column :stalls, :name, :string, :null => false
    change_column :stalls, :seq, :integer, :null => false
  end
end
