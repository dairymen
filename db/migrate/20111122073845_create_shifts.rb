class CreateShifts < ActiveRecord::Migration
  def change
    create_table :shifts do |t|
      t.references :cow
      t.references :stall
      t.date :date

      t.timestamps
    end
    add_index :shifts, :cow_id
    add_index :shifts, :stall_id
  end
end
