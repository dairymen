class CreateStalls < ActiveRecord::Migration
  def change
    create_table :stalls do |t|
      t.string :name
      t.integer :seq

      t.timestamps
    end

    change_column :kine, :cage, :integer
    rename_column :kine, :cage, :stall_id
  end
end
