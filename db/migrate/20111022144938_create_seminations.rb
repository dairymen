class CreateSeminations < ActiveRecord::Migration
  def change
    create_table :seminations do |t|
      t.integer :cow_id
      t.integer :estrus_id
      t.date :date
      t.string :bull

      t.timestamps
    end
  end
end
