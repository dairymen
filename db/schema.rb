# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120124150700) do

  create_table "estruses", :force => true do |t|
    t.integer  "cow_id",     :null => false
    t.date     "date",       :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "kine", :force => true do |t|
    t.string   "ear_tag",         :limit => 16,  :null => false
    t.integer  "stall_id",        :limit => 255, :null => false
    t.date     "birthday",                       :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "approval_number",                :null => false
  end

  create_table "plans", :force => true do |t|
    t.date     "start",      :null => false
    t.date     "end"
    t.text     "content",    :null => false
    t.integer  "cow_id",     :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "seminations", :force => true do |t|
    t.integer  "cow_id",                   :null => false
    t.integer  "estrus_id"
    t.date     "date",                     :null => false
    t.string   "bull",       :limit => 32, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "shifts", :force => true do |t|
    t.integer  "cow_id"
    t.integer  "stall_id"
    t.date     "date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "shifts", ["cow_id"], :name => "index_shifts_on_cow_id"
  add_index "shifts", ["stall_id"], :name => "index_shifts_on_stall_id"

  create_table "stalls", :force => true do |t|
    t.string   "name",       :null => false
    t.integer  "seq",        :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
