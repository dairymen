#
# DateHelperJa
#
# Japanizes ActionView::Helpers::DateHelper.
#
# Released under the MIT license
# Copyright (c) Eiji Sakai <eiji.sakai@softculture.com>
# http://d.hatena.ne.jp/elm200/
# 

PLUGIN_NAME = 'date_helper_ja'
PLUGIN_PATH = File.dirname(__FILE__) + "/lib/" + PLUGIN_NAME + ".rb"
require_dependency PLUGIN_NAME if defined? ActionView::Base

