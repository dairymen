class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :set_locale

  helper_method :next_direction, :next_params

  private
  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  def redirect_with_notice(model, msg)
    redirect_to model, notice: t(msg, :scope => 'helpers.messages', :model => (model.is_a?(Enumerable) ? model.last : model).class.model_name.human)
  end

  def self.default_sort_column(name)
    class_eval %{
      def default_sort_column
        :#{name}.to_s
      end
    }
  end

  def sort_column
    session[controller_name.to_sym] ||= {}
    session[controller_name.to_sym][:sort_column] = params[:o] if params.key? :o
    if self.class.method_defined? :default_sort_column
      session[controller_name.to_sym][:sort_column] || default_sort_column || "id"
    else
      session[controller_name.to_sym][:sort_column] || "id"
    end
  end

  def direction
    session[controller_name.to_sym] ||= {}
    session[controller_name.to_sym][:direction] = params[:d] if params.key? :d
    session[controller_name.to_sym][:direction] || "desc" 
  end

  def next_params(name)
    if sort_column == params[:o]
      d = next_direction
    else
      d = "asc"
    end
    params.merge(:o => name, :d => d)
  end

  def next_direction
    direction == "desc" ? "asc" : "desc"
  end

end
