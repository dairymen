class SeminationsController < ApplicationController
  # GET /seminations
  # GET /seminations.json
  def index
    @cow = Cow.find(params[:cow_id])
    @seminations = Semination.where(:cow_id => params[:cow_id])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @seminations }
    end
  end

  # GET /seminations/1
  # GET /seminations/1.json
  def show
    @cow = Cow.find(params[:cow_id])
    @semination = Semination.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @semination }
    end
  end

  # GET /seminations/new
  # GET /seminations/new.json
  def new
    @cow = Cow.find(params[:cow_id])
    @semination = Semination.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @semination }
    end
  end

  # GET /seminations/1/edit
  def edit
    @cow = Cow.find(params[:cow_id])
    @semination = Semination.find(params[:id])
  end

  # POST /seminations
  # POST /seminations.json
  def create
    @cow = Cow.find(params[:cow_id])
    @semination = Semination.new(params[:semination])
    @cow.seminations << @semination

    respond_to do |format|
      if @semination.save
        format.html { redirect_with_notice [@cow, @semination], :created }
        format.json { render json: @semination, status: :created, location: @semination }
      else
        format.html { render action: "new" }
        format.json { render json: @semination.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /seminations/1
  # PUT /seminations/1.json
  def update
    @cow = Cow.find(params[:cow_id])
    @semination = Semination.find(params[:id])

    respond_to do |format|
      if @semination.update_attributes(params[:semination])
        format.html { redirect_to [@cow, @semination], notice: 'Semination was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @semination.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /seminations/1
  # DELETE /seminations/1.json
  def destroy
    @cow = Cow.find(params[:cow_id])
    @semination = Semination.find(params[:id])
    @semination.destroy

    respond_to do |format|
      format.html { redirect_to cow_seminations_url }
      format.json { head :ok }
    end
  end
end
