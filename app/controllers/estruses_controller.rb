class EstrusesController < ApplicationController
  # GET /estruses
  # GET /estruses.json
  def index
    @estruses = Estrus.where(:cow_id => params[:cow_id])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @estruses }
    end
  end

  # GET /estruses/1
  # GET /estruses/1.json
  def show
    @cow = Cow.find(params[:cow_id])
    @estrus = Estrus.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @estrus }
    end
  end

  # GET /estruses/new
  # GET /estruses/new.json
  def new
    @cow = Cow.find(params[:cow_id])
    @estrus = Estrus.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @estrus }
    end
  end

  # GET /estruses/1/edit
  def edit
    @cow = Cow.find(params[:cow_id])
    @estrus = Estrus.find(params[:id])
  end

  # POST /estruses
  # POST /estruses.json
  def create
    @cow = Cow.find(params[:cow_id])
    @estrus = Estrus.new(params[:estrus])
    @estrus.cow = @cow

    respond_to do |format|
      if @estrus.save
        format.html { redirect_with_notice [@cow, @estrus], :created }
        format.json { render json: @estrus, status: :created, location: @estrus }
      else
        format.html { render action: "new" }
        format.json { render json: @estrus.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /estruses/1
  # PUT /estruses/1.json
  def update
    @cow = Cow.find(params[:cow_id])
    @estrus = Estrus.find(params[:id])

    respond_to do |format|
      if @estrus.update_attributes(params[:estrus])
        format.html { redirect_to [@cow, @estrus], notice: 'Estrus was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @estrus.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /estruses/1
  # DELETE /estruses/1.json
  def destroy
    @cow = Cow.find(params[:cow_id])
    @estrus = Estrus.find(params[:id])
    @estrus.destroy

    respond_to do |format|
      format.html { redirect_to cow_estruses_url(@cow) }
      format.json { head :ok }
    end
  end
end
