class KineController < ApplicationController
  default_sort_column :stall
  # GET /kine
  # GET /kine.json
  def index
    @search = Cow.search(params[:search])
    @kine = @search.page params[:page]

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @kine }
    end
  end

  # GET /kine/1
  # GET /kine/1.json
  def show
    @cow = Cow.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @cow }
    end
  end

  # GET /kine/new
  # GET /kine/new.json
  def new
    @cow = Cow.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @cow }
    end
  end

  # GET /kine/1/edit
  def edit
    @cow = Cow.find(params[:id])
  end

  # POST /kine
  # POST /kine.json
  def create
    @cow = Cow.new(params[:cow])
    if @cow.stall
      @shift = Shift.new
      @shift.cow = @cow
      @shift.stall = @cow.stall
      @shift.date = Date.today
    end

    respond_to do |format|
      Cow.transaction do
        if @cow.save and @shift.save
          format.html { redirect_with_notice @cow, :created }
          format.json { render json: @cow, status: :created, location: @cow }
        else
          format.html { render action: "new" }
          format.json { render json: @cow.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PUT /kine/1
  # PUT /kine/1.json
  def update
    @cow = Cow.find(params[:id])

    respond_to do |format|
      if @cow.update_attributes(params[:cow])
        format.html { redirect_to @cow, notice: 'Cow was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @cow.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /kine/1
  # DELETE /kine/1.json
  def destroy
    @cow = Cow.find(params[:id])
    @cow.destroy

    respond_to do |format|
      format.html { redirect_to kine_url }
      format.json { head :ok }
    end
  end
end
