class Stall < ActiveRecord::Base
  has_many :cow
  has_many :shifts

  validates_presence_of :name
  validates :seq, :presence => true, :numericality => { :only_integer => true }, :uniqueness => true, :allow_blank => true
end
