class Estrus < ActiveRecord::Base
  belongs_to :cow
  has_one :semination
  def next
    21.days.since self.date
  end
end
