class Cow < ActiveRecord::Base
  belongs_to :stall
  has_many :estruses
  has_many :seminations
  has_many :shifts
  has_many :plans

  validates :ear_tag, :presence => true, :uniqueness => true, :length => {:is => 10}, :numericality => true
end
