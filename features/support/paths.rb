# -*- coding: utf-8 -*-
module NavigationHelpers
  # Maps a name to a path. Used by the
  #
  # When /^I go to (.+)$/ do |page_name|
  #
  # step definition
  #
  def path_to(page_name)
    case page_name

    when /^the home\s?page$/
      '/'
    when /^牛登録$/
      '/kine/new'
    when /^場所登録$/
      '/stalls/new'
    when /^id:(\d+)の発情登録$/
      "/kine/#{$1}/estruses/new"
    when /^id:(\d+)の授精登録$/
      "/kine/#{$1}/seminations/new"
    when /^異動$/
      "/shifts/new"
    when /^異動一覧$/
      "/shifts"

    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    # when /^(.*)'s profile page$/i
    # user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /^the (.*) page$/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue NoMethodError, ArgumentError
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
end

World(NavigationHelpers)
