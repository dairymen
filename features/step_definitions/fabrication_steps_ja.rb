# -*- coding: utf-8 -*-

Given /^"([^\"]*)"が(\d+)個ある$/ do |model_name, count|
  step %Q(#{count} #{model_name})
end

Given /^以下の"([^\"]*)"を準備する:$/ do |model_name, table|
   step %Q(the following #{model_name}:), table
end 

Given /^"([^\"]*)"は以下の"([^\"]*)"を持っている:$/ do |parent, child, table|
  step %Q(that #{parent} has the following #{child}:), table
end

Given /^"([^\"]*)"は"([^\"]*)"を(\d+)個持っている$/ do |parent, child, count|
  step %Q(that #{parent} has #{count} #{child})
end

Given /^"([^\"]*)"は"([^\"]*)"に属している$/ do |children, parent|
  step %Q(that #{children} belongs to that #{parent})
end

Given /^データベースに(\d+)件の"([^\"]*)"がある$/ do |count, model_name|
  step %Q(I should see #{count} #{model_name} in the database)
end

Given /^データベースに以下の"([^\"]*)"がある:$/ do |model_name, table|
  step %Q(I should see the following #{model_name} in the database:), table
end
