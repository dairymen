Fabricator(:cow) do
  ear_tag { Faker::PhoneNumber.phone_number }
  stall_id 1
  birthday { Date.parse("2010-#{rand(12)+1}-#{rand(30)+1}") }
  approval_number { Fabricate.sequence(:approval_number, 1) }
end

Fabricator(:mium, :from => :cow) do
  ear_tag "1234567809"
  stall(:fabricator => :west1)
  birthday Date.parse("2011-10-10")
end

Fabricator(:sequed_cow, :from => :cow) {
  ear_tag { Fabricate.sequence(:ear_tag, 5000000000) }
  birthday { Fabricate.sequence(:birthday){|n| n.days.since } }
}
