Fabricator(:estrus) do
  cow!
  date "2011-10-22"
end

Fabricator(:may2009, :from => :estrus) do
  cow(Fabricate(:mium))
  date Date.parse("2009-05-02")
end
