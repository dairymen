# -*- coding: utf-8 -*-
# 日本語のファイル
Fabricator(:stall) do
  name { Faker::Address.city }
  seq { Fabricate.seqence(:seq, 1) }
end

Fabricator(:west1, :from => :stall) do
  name "西1"
  seq 1
end

Fabricator(:sequed_stall, :from => :stall) do
  name { Fabricate.sequence(:name){|n| "stall#{n}" } }
  seq { Fabricate.sequence(:seq, 1) }
end
