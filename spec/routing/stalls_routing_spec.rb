require "spec_helper"

describe StallsController do
  describe "routing" do

    it "routes to #index" do
      get("/stalls").should route_to("stalls#index")
    end

    it "routes to #new" do
      get("/stalls/new").should route_to("stalls#new")
    end

    it "routes to #show" do
      get("/stalls/1").should route_to("stalls#show", :id => "1")
    end

    it "routes to #edit" do
      get("/stalls/1/edit").should route_to("stalls#edit", :id => "1")
    end

    it "routes to #create" do
      post("/stalls").should route_to("stalls#create")
    end

    it "routes to #update" do
      put("/stalls/1").should route_to("stalls#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/stalls/1").should route_to("stalls#destroy", :id => "1")
    end

  end
end
