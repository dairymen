require "spec_helper"

describe SeminationsController do
  describe "routing" do

    it "routes to #index" do
      get("/seminations").should route_to("seminations#index")
    end

    it "routes to #new" do
      get("/seminations/new").should route_to("seminations#new")
    end

    it "routes to #show" do
      get("/seminations/1").should route_to("seminations#show", :id => "1")
    end

    it "routes to #edit" do
      get("/seminations/1/edit").should route_to("seminations#edit", :id => "1")
    end

    it "routes to #create" do
      post("/seminations").should route_to("seminations#create")
    end

    it "routes to #update" do
      put("/seminations/1").should route_to("seminations#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/seminations/1").should route_to("seminations#destroy", :id => "1")
    end

  end
end
