require "spec_helper"

describe EstrusesController do
  describe "routing" do

    it "routes to #index" do
      get("/estruses").should route_to("estruses#index")
    end

    it "routes to #new" do
      get("/estruses/new").should route_to("estruses#new")
    end

    it "routes to #show" do
      get("/estruses/1").should route_to("estruses#show", :id => "1")
    end

    it "routes to #edit" do
      get("/estruses/1/edit").should route_to("estruses#edit", :id => "1")
    end

    it "routes to #create" do
      post("/estruses").should route_to("estruses#create")
    end

    it "routes to #update" do
      put("/estruses/1").should route_to("estruses#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/estruses/1").should route_to("estruses#destroy", :id => "1")
    end

  end
end
