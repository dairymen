require 'spec_helper'

describe "shifts/index.html.haml" do
  before(:each) do
    assign(:shifts, [
      stub_model(Shift,
        :cow => nil,
        :stall => nil
      ),
      stub_model(Shift,
        :cow => nil,
        :stall => nil
      )
    ])
  end

  it "renders a list of shifts" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
