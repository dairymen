require 'spec_helper'

describe "shifts/new.html.haml" do
  before(:each) do
    assign(:shift, stub_model(Shift,
      :cow => nil,
      :stall => nil
    ).as_new_record)
  end

  it "renders new shift form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => shifts_path, :method => "post" do
      assert_select "input#shift_cow", :name => "shift[cow]"
      assert_select "input#shift_stall", :name => "shift[stall]"
    end
  end
end
