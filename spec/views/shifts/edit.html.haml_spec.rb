require 'spec_helper'

describe "shifts/edit.html.haml" do
  before(:each) do
    @shift = assign(:shift, stub_model(Shift,
      :cow => nil,
      :stall => nil
    ))
  end

  it "renders the edit shift form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => shifts_path(@shift), :method => "post" do
      assert_select "input#shift_cow", :name => "shift[cow]"
      assert_select "input#shift_stall", :name => "shift[stall]"
    end
  end
end
