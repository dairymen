require 'spec_helper'

describe "estruses/edit.html.haml" do
  before(:each) do
    @estrus = assign(:estrus, stub_model(Estrus,
      :cow_id => 1
    ))
  end

  it "renders the edit estrus form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => estruses_path(@estrus), :method => "post" do
      assert_select "input#estru_cow_id", :name => "estrus[cow_id]"
    end
  end
end
