require 'spec_helper'

describe "estrus/new.html.haml" do
  before(:each) do
    assign(:estrus, stub_model(Estrus,
      :cow_id => 1
    ).as_new_record)
  end

  it "renders new estrus form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => estrus_path, :method => "post" do
      assert_select "input#estru_cow_id", :name => "estrus[cow_id]"
    end
  end
end
