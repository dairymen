require 'spec_helper'

describe "estrus/show.html.haml" do
  before(:each) do
    @estru = assign(:estru, stub_model(Estru,
      :cow_id => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
  end
end
