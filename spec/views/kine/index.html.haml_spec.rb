require 'spec_helper'

describe "kine/index.html.haml" do
  before(:each) do
    assign(:kine, [
      stub_model(Cow,
        :ear_tag => "Ear Tag",
        :cage => "Cage"
      ),
      stub_model(Cow,
        :ear_tag => "Ear Tag",
        :cage => "Cage"
      )
    ])
  end

  it "renders a list of kine" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Ear Tag".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Cage".to_s, :count => 2
  end
end
