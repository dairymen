require 'spec_helper'

describe "kine/edit.html.haml" do
  before(:each) do
    @cow = assign(:cow, stub_model(Cow,
      :ear_tag => "MyString",
      :cage => "MyString"
    ))
  end

  it "renders the edit cow form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => kine_path(@cow), :method => "post" do
      assert_select "input#cow_ear_tag", :name => "cow[ear_tag]"
      assert_select "input#cow_cage", :name => "cow[cage]"
    end
  end
end
