require 'spec_helper'

describe "kine/show.html.haml" do
  before(:each) do
    @cow = assign(:cow, stub_model(Cow,
      :ear_tag => "Ear Tag",
      :cage => "Cage"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Ear Tag/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Cage/)
  end
end
