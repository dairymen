require 'spec_helper'

describe "plans/index.html.haml" do
  before(:each) do
    assign(:plans, [
      stub_model(Plan,
        :content => "MyText"
      ),
      stub_model(Plan,
        :content => "MyText"
      )
    ])
  end

  it "renders a list of plans" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
