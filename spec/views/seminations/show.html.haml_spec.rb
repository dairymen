require 'spec_helper'

describe "seminations/show.html.haml" do
  before(:each) do
    @semination = assign(:semination, stub_model(Semination,
      :cow_id => 1,
      :estrus_id => 1,
      :bull => "Bull"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Bull/)
  end
end
