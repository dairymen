require 'spec_helper'

describe "seminations/edit.html.haml" do
  before(:each) do
    @semination = assign(:semination, stub_model(Semination,
      :cow_id => 1,
      :estrus_id => 1,
      :bull => "MyString"
    ))
  end

  it "renders the edit semination form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => seminations_path(@semination), :method => "post" do
      assert_select "input#semination_cow_id", :name => "semination[cow_id]"
      assert_select "input#semination_estrus_id", :name => "semination[estrus_id]"
      assert_select "input#semination_bull", :name => "semination[bull]"
    end
  end
end
