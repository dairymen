require 'spec_helper'

describe "seminations/index.html.haml" do
  before(:each) do
    assign(:seminations, [
      stub_model(Semination,
        :cow_id => 1,
        :estrus_id => 1,
        :bull => "Bull"
      ),
      stub_model(Semination,
        :cow_id => 1,
        :estrus_id => 1,
        :bull => "Bull"
      )
    ])
  end

  it "renders a list of seminations" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Bull".to_s, :count => 2
  end
end
