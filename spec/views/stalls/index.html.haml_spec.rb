require 'spec_helper'

describe "stalls/index.html.haml" do
  before(:each) do
    assign(:stalls, [
      stub_model(Stall,
        :name => "Name",
        :seq => 1
      ),
      stub_model(Stall,
        :name => "Name",
        :seq => 1
      )
    ])
  end

  it "renders a list of stalls" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
