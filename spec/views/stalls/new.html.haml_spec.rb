require 'spec_helper'

describe "stalls/new.html.haml" do
  before(:each) do
    assign(:stall, stub_model(Stall,
      :name => "MyString",
      :seq => 1
    ).as_new_record)
  end

  it "renders new stall form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => stalls_path, :method => "post" do
      assert_select "input#stall_name", :name => "stall[name]"
      assert_select "input#stall_seq", :name => "stall[seq]"
    end
  end
end
