require 'spec_helper'

describe "stalls/edit.html.haml" do
  before(:each) do
    @stall = assign(:stall, stub_model(Stall,
      :name => "MyString",
      :seq => 1
    ))
  end

  it "renders the edit stall form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => stalls_path(@stall), :method => "post" do
      assert_select "input#stall_name", :name => "stall[name]"
      assert_select "input#stall_seq", :name => "stall[seq]"
    end
  end
end
