require 'spec_helper'

describe "stalls/show.html.haml" do
  before(:each) do
    @stall = assign(:stall, stub_model(Stall,
      :name => "Name",
      :seq => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
  end
end
