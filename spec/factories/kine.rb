# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :cow do
      ear_tag "MyString"
      cage "MyString"
      birthday "2011-10-10"
    end
end